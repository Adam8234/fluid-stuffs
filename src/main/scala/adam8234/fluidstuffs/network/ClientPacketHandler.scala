package adam8234.fluidstuffs.network

import adam8234.fluidstuffs.block.TileEntityFluidTransporter
import adam8234.fluidstuffs.util.LogHelper
import codechicken.lib.packet.PacketCustom
import codechicken.lib.packet.PacketCustom.IClientPacketHandler
import net.minecraft.client.Minecraft
import net.minecraft.network.play.INetHandlerPlayClient

class ClientPacketHandler extends IClientPacketHandler {
  override def handlePacket(p1: PacketCustom, p2: Minecraft, p3: INetHandlerPlayClient): Unit = {
    p1.getType match {
      case 1 => {
        val coord = p1.readCoord()
        val tile = p2.theWorld.getTileEntity(coord.x, coord.y, coord.z)
        if (tile.isInstanceOf[TileEntityFluidTransporter]) {
          tile.asInstanceOf[TileEntityFluidTransporter].syncTank.sync(p1.readFluidStack())
        }
      }
    }
  }

}

object ClientPacketHandler {
  val channel = "FluidStuffs"
}
