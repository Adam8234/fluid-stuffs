package adam8234.fluidstuffs.handler

import adam8234.fluidstuffs.block.TileEntityFluidTransporter
import adam8234.fluidstuffs.client.GuiFluidTransporter
import adam8234.fluidstuffs.inventory.ContainerFluidTransporter
import cpw.mods.fml.common.network.IGuiHandler
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.world.World

class GuiHandler extends IGuiHandler {
  override def getServerGuiElement(ID: Int, player: EntityPlayer, world: World, x: Int, y: Int, z: Int): AnyRef = {
    if (ID == GUIIds.FLUID_TRANSPORTER) {
      return new ContainerFluidTransporter
    }
    null
  }

  override def getClientGuiElement(ID: Int, player: EntityPlayer, world: World, x: Int, y: Int, z: Int): AnyRef = {
    if (ID == GUIIds.FLUID_TRANSPORTER) {
      val tile = world.getTileEntity(x, y, z).asInstanceOf[TileEntityFluidTransporter]
      return new GuiFluidTransporter(tile)
    } else null
  }
}

object GUIIds {
  val FLUID_TRANSPORTER = 0
}
