package adam8234.fluidstuffs.core

import java.util

import adam8234.fluidstuffs.core.lib.Enum
import cpw.mods.fml.common.registry.GameRegistry
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.{Item, ItemStack}

abstract class ItemCore(name: String) extends Item {
  setUnlocalizedName(name)
  GameRegistry.registerItem(this, name)

  override def getUnlocalizedName(stack: ItemStack): String = if (hasSubtypes) getUnlocalizedName() + "|" + stack.getItemDamage else getUnlocalizedName()
}

abstract class ItemEnum extends Enum {
  type EnumVal <: ItemDef

  def getItem: Item

  class ItemDef extends Value {
    val meta = ordinal;

    override def name: String = getItem getUnlocalizedName (makeStack)

    def makeStack: ItemStack = makeStack(1)

    def makeStack(i: Int) = new ItemStack(getItem, i, meta)
  }

}
