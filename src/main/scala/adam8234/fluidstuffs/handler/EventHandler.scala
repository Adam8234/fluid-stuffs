package adam8234.fluidstuffs.handler

import adam8234.fluidstuffs.block.TileEntityFluidTransporter
import adam8234.fluidstuffs.item.ItemTankBinder
import adam8234.fluidstuffs.util.LogHelper
import codechicken.lib.math.MathHelper
import codechicken.lib.vec.Vector3
import cpw.mods.fml.common.eventhandler.SubscribeEvent
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.{Vec3, MovingObjectPosition}
import net.minecraft.util.MovingObjectPosition.MovingObjectType
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.event.entity.player.PlayerInteractEvent
import net.minecraftforge.fluids.IFluidHandler

class EventHandler {
  @SubscribeEvent
  def onPlayerRightClick(e: PlayerInteractEvent): Unit = if (e.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) onPlayerRightClickBlock(e)

  def onPlayerRightClickBlock(e: PlayerInteractEvent) = {
    val player = e.entityPlayer
    val world = player.getEntityWorld
    val block = world.getBlock(e.x, e.y, e.z)
    val itemStack = player.inventory.getCurrentItem
    def getSide(reach: Double) = {
      def getMovingObjectPositionFromPlayer(flag: Boolean): MovingObjectPosition = {
        val f = 1.0F;
        val playerPitch = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * f;
        val playerYaw = player.prevRotationYaw + (player.rotationYaw - player.prevRotationYaw) * f;
        val playerPosX = player.prevPosX + (player.posX - player.prevPosX) * f;
        val playerPosY = (player.prevPosY + (player.posY - player.prevPosY) * f + 1.6200000000000001D) - player.yOffset;
        val playerPosZ = player.prevPosZ + (player.posZ - player.prevPosZ) * f;
        val vecPlayer = Vec3.createVectorHelper(playerPosX, playerPosY, playerPosZ);
        val cosYaw = MathHelper.cos(-playerYaw * 0.01745329F - 3.141593F);
        val sinYaw = MathHelper.sin(-playerYaw * 0.01745329F - 3.141593F);
        val cosPitch = -MathHelper.cos(-playerPitch * 0.01745329F);
        val sinPitch = MathHelper.sin(-playerPitch * 0.01745329F);
        val pointX = sinYaw * cosPitch;
        val pointY = sinPitch;
        val pointZ = cosYaw * cosPitch;
        val vecPoint = vecPlayer.addVector(pointX * reach, pointY * reach, pointZ * reach);
        val movingobjectposition = world.rayTraceBlocks(vecPlayer, vecPoint, flag);
        return movingobjectposition;
      }
      var blockInfo = -1
      val movingobjectposition = getMovingObjectPositionFromPlayer(true);
      if (movingobjectposition != null) {
        if (movingobjectposition.typeOfHit == MovingObjectType.BLOCK) {
          blockInfo = movingobjectposition.sideHit;
        }
      }
      blockInfo
    }
    if (itemStack != null && itemStack.getItem.isInstanceOf[ItemTankBinder]) {
      if (player.isSneaking && world.getTileEntity(e.x, e.y, e.z) != null && world.getTileEntity(e.x, e.y, e.z).isInstanceOf[IFluidHandler]) {
        if (!itemStack.hasTagCompound) {
          itemStack.stackTagCompound = new NBTTagCompound
        }
        itemStack.stackTagCompound.setInteger("x", e.x)
        itemStack.stackTagCompound.setInteger("y", e.y)
        itemStack.stackTagCompound.setInteger("z", e.z)
        itemStack.stackTagCompound.setInteger("side", getSide(8D))
        itemStack.stackTagCompound.setString("boundName", world.getBlock(e.x, e.y, e.z).getUnlocalizedName)

      } else {
        if(world.getTileEntity(e.x, e.y, e.z).isInstanceOf[TileEntityFluidTransporter]){
          if (!itemStack.hasTagCompound) {
            itemStack.stackTagCompound = new NBTTagCompound
          }
          val tile = world.getTileEntity(e.x, e.y, e.z).asInstanceOf[TileEntityFluidTransporter]
          if(tile.target == null){
            tile.target = new Vector3()
          }
          tile.target.set(itemStack.stackTagCompound.getInteger("x"), itemStack.stackTagCompound.getInteger("y"), itemStack.stackTagCompound.getInteger("z"))
          tile.targetSide = ForgeDirection.VALID_DIRECTIONS.apply(itemStack.stackTagCompound.getInteger("side"))
        }
      }
    }
  }

}
