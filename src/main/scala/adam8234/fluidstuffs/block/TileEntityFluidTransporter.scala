package adam8234.fluidstuffs.block

import adam8234.fluidstuffs.network.ServerPacketHandler
import adam8234.fluidstuffs.util.LogHelper
import codechicken.lib.packet.PacketCustom
import codechicken.lib.vec.Vector3
import cofh.util.MathHelper
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.Packet
import net.minecraft.tileentity.TileEntity
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.fluids._
import sun.rmi.runtime.Log

class TileEntityFluidTransporter extends TileEntity with IFluidHandler {
  var syncTank = new Tank
  var target: Vector3 = null
  var targetSide: ForgeDirection = ForgeDirection.UNKNOWN
  var clientAmount = 0.0
  var SYNC_TIME = 8
  var ticksSinceLastSync = hashCode() % SYNC_TIME
  var needsSync = false
  var forceUpdate = true

  override def fill(from: ForgeDirection, resource: FluidStack, doFill: Boolean): Int = {
    if (worldObj.isRemote) return 0
    val ret = syncTank.fill(resource, doFill)
    if (ret > 0 && doFill) forceUpdate = true
    //syncTank.sendSync()
    ret
  }

  override def drain(from: ForgeDirection, resource: FluidStack, doDrain: Boolean): FluidStack = {
    if (worldObj.isRemote) return null
    val ret = syncTank.drain(resource.amount, doDrain)
    if (ret != null && doDrain) forceUpdate = true
    //syncTank.sendSync()
    ret
  }

  override def drain(from: ForgeDirection, maxDrain: Int, doDrain: Boolean): FluidStack = {
    if (worldObj.isRemote) return null
    val ret = syncTank.drain(maxDrain, doDrain)
    if (ret != null && doDrain) forceUpdate = true
    //syncTank.sendSync()
    ret
  }


  override def updateEntity() {
    ticksSinceLastSync += 1
    if (!worldObj.isRemote && forceUpdate) {
      forceUpdate = false
      needsSync = true
      syncTank.sendSync()
    }
    if (needsSync && !worldObj.isRemote && ticksSinceLastSync > SYNC_TIME) {
      needsSync = false
      syncTank.sendSync()
      ticksSinceLastSync = 0
    }

    if(worldObj.isRemote){
      if(syncTank.containesFluid())
        clientAmount = MathHelper.approachExp(clientAmount, syncTank.getInfo.fluid.amount, 0.1)
      else
        clientAmount = 0
    }

  }

  override def canFill(from: ForgeDirection, fluid: Fluid): Boolean = true

  override def canDrain(from: ForgeDirection, fluid: Fluid): Boolean = true

  override def getTankInfo(from: ForgeDirection): Array[FluidTankInfo] = Array(syncTank.getInfo)

  override def readFromNBT(nbt: NBTTagCompound): Unit = {
    super.readFromNBT(nbt)
    syncTank.readFromNBT(nbt)
  }

  override def writeToNBT(nbt: NBTTagCompound): Unit = {
    super.writeToNBT(nbt)
    syncTank.writeToNBT(nbt)
  }


  override def getDescriptionPacket: Packet = {
    val packet = new PacketCustom(ServerPacketHandler.channel, 1)
    packet.writeCoord(xCoord, yCoord, zCoord)
    packet.writeFluidStack(syncTank.getFluid)
    packet.toPacket
  }

  class Tank extends SyncronizedTank(new FluidTank(16 * FluidContainerRegistry.BUCKET_VOLUME)) {
    override def sendSync() {
      val packet = new PacketCustom(ServerPacketHandler.channel, 1)
      packet.writeCoord(xCoord, yCoord, zCoord)
      packet.writeFluidStack(syncTank.getFluid())
      packet.sendToChunk(worldObj, xCoord >> 4, zCoord >> 4)
    }

    override def onFluidChanged() {
      worldObj.func_147451_t(xCoord, yCoord, zCoord)
    }
  }

}
