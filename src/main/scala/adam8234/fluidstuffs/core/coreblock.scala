package adam8234.fluidstuffs.core

import cpw.mods.fml.common.registry.GameRegistry
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.{Item, ItemBlock, ItemStack}
import net.minecraft.tileentity.TileEntity

class CoreBlock(name: String, mat: Material) extends Block(mat) {
  setBlockName(name);
  GameRegistry registerBlock(this, getItemBlockClass, name)
  setCreativeTab(CreativeTabs tabBlock)

  def getItemBlockClass: Class[_ <: ItemBlock] = classOf[ItemBlockCore]

  def registerTile[A <: TileEntity](c: Class[A]) = GameRegistry registerTileEntity(c, getUnlocalizedName)
}

class ItemBlockCore(block: Block) extends ItemBlock(block) {
  setHasSubtypes(true)
  setMaxDamage(0)

  override def getMetadata(meta: Int) = meta

  override def getUnlocalizedName(stack: ItemStack) = super.getUnlocalizedName + "|" + stack.getItemDamage
}

abstract class BlockEnum extends ItemEnum {
  override type EnumVal <: BlockDef

  override def getItem = Item.getItemFromBlock(getBlock)

  def getBlock: Block

  class BlockDef extends ItemDef

}

