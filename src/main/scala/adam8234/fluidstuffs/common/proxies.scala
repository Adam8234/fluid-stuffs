package adam8234.fluidstuffs.common

import adam8234.fluidstuffs.handler.EventHandler
import codechicken.lib.packet.PacketCustom
import adam8234.fluidstuffs.block.{TileEntityFluidTransporter, FluidTransporter}
import adam8234.fluidstuffs.client.RenderFluidTransporter
import adam8234.fluidstuffs.item.ItemTankBinder
import adam8234.fluidstuffs.network.{ServerPacketHandler, ClientPacketHandler}
import cpw.mods.fml.client.registry.{RenderingRegistry, ClientRegistry}
import cpw.mods.fml.relauncher.{Side, SideOnly}
import net.minecraftforge.common.MinecraftForge


class Proxy_server extends IProxy {
  override def preInit() {
    ModBlocks fluidTransporter = new FluidTransporter
    new ItemTankBinder
  }

  override def init() {
    PacketCustom assignHandler(ClientPacketHandler.channel, new ServerPacketHandler)
  }

  override def postInit() {

  }
}

class Proxy_client extends Proxy_server {
  @SideOnly(Side.CLIENT)
  override def postInit() {
    var fluidTransporter = new RenderFluidTransporter
    RenderingRegistry.registerBlockHandler(fluidTransporter)
    ClientRegistry.bindTileEntitySpecialRenderer(classOf[TileEntityFluidTransporter], fluidTransporter)
    super.postInit()
  }

  @SideOnly(Side.CLIENT)
  override def init() {
    PacketCustom assignHandler(ClientPacketHandler.channel, new ClientPacketHandler)
    super.init()
  }

  @SideOnly(Side.CLIENT)
  override def preInit() {
    super.preInit()
  }
}

object Proxy extends Proxy_client

trait IProxy {
  def preInit()

  def init()

  def postInit()
}
