package adam8234.fluidstuffs.network

import codechicken.lib.packet.PacketCustom
import codechicken.lib.packet.PacketCustom.IServerPacketHandler
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.network.play.INetHandlerPlayServer

class ServerPacketHandler extends IServerPacketHandler {
  override def handlePacket(packet: PacketCustom, sender: EntityPlayerMP, handler: INetHandlerPlayServer): Unit = {}
}

object ServerPacketHandler {
  val channel = "FluidStuffs"
}
