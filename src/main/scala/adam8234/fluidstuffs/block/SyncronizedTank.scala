package adam8234.fluidstuffs.block

import adam8234.fluidstuffs.util.LogHelper
import cofh.util.MathHelper
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.fluids._

abstract class SyncronizedTank(tank: FluidTank) extends IFluidTank {

  def containsFluid() = tank.getFluid != null

  def sendSync()

  def onFluidChanged()

  def sync(fluid: FluidStack) {
    //Client based
    LogHelper info "Client Sync"
    tank.setFluid(fluid)
  }

  def getFluid() = tank.getFluid

  def containesFluid() = getFluid != null

  override def getFluidAmount: Int = tank.getFluidAmount

  override def drain(maxDrain: Int, doDrain: Boolean): FluidStack = tank.drain(maxDrain, doDrain)

  override def getInfo: FluidTankInfo = tank.getInfo

  override def getCapacity: Int = tank.getCapacity

  override def fill(resource: FluidStack, doFill: Boolean): Int = tank.fill(resource, doFill)

  def writeToNBT(nbt:NBTTagCompound):NBTTagCompound = tank.writeToNBT(nbt)

  def readFromNBT(nbt:NBTTagCompound):FluidTank = tank.readFromNBT(nbt)
}

