package adam8234.fluidstuffs.util

import codechicken.lib.vec.Vector3
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntity
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.fluids.{FluidStack, IFluidHandler}

class FluidTileDef(pos: Vector3, item: ItemStack, tile: TileEntity) {
  def getTile(): TileEntity = tile

  def getPos(): Vector3 = pos

  def getItemStack(): ItemStack = item

  def fillFluid(direction: ForgeDirection, fluidStack: FluidStack) =
    tile.asInstanceOf[IFluidHandler] fill(direction, fluidStack, true)

  def getBlockName(): String = tile.getWorldObj.getBlock(tile.xCoord, tile.yCoord, tile.zCoord).getLocalizedName
}