package adam8234.fluidstuffs.client

import cofh.gui.GuiBase
import cofh.gui.element.ElementFluidTank
import adam8234.fluidstuffs.block.TileEntityFluidTransporter
import adam8234.fluidstuffs.inventory.ContainerFluidTransporter
import net.minecraft.client.gui.GuiButton
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fluids.FluidRegistry

class GuiFluidTransporter(tile: TileEntityFluidTransporter) extends GuiBase(new ContainerFluidTransporter, new ResourceLocation("minecraft:textures/gui/demo_background.png")) {
  xSize = 248
  ySize = 166
  drawInventory = false

  override def initGui(): Unit = {
    super.initGui()
  }

  override protected def drawGuiContainerForegroundLayer(x: Int, y: Int): Unit = {
    super.drawGuiContainerForegroundLayer(x, y)
  }
}
