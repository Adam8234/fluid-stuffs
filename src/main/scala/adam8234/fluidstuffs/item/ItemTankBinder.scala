package adam8234.fluidstuffs.item

import java.util.{List => JList}

import adam8234.fluidstuffs.core.ItemCore
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.util.{StatCollector, IIcon}

class ItemTankBinder extends ItemCore("itemTankBinder") {
  override def getIcon(stack: ItemStack, renderPass: Int, player: EntityPlayer, usingItem: ItemStack, useRemaining: Int): IIcon = if (stack.hasTagCompound && stack.stackTagCompound.getBoolean("bound")) itemIcon else itemIcon

  override def addInformation(stack: ItemStack, player: EntityPlayer, _list: JList[_], par4: Boolean): Unit = {
    val list = _list.asInstanceOf[JList[String]]
    if (stack.hasTagCompound) {
      val x = stack.stackTagCompound.getInteger("x")
      val y = stack.stackTagCompound.getInteger("y")
      val z = stack.stackTagCompound.getInteger("z")
      list.add(s"X: $x,Y: $y, Z: $z")
      list.add(StatCollector.translateToLocal(stack.stackTagCompound.getString("boundName")))
    } else {
      list.add("Not Bound Yet")
    }
  }
}
