package adam8234.fluidstuffs.block

import adam8234.fluidstuffs.client.RenderFluidTransporter
import adam8234.fluidstuffs.core.CoreBlock
import adam8234.fluidstuffs.FluidStuffs
import adam8234.fluidstuffs.handler.GUIIds
import cofh.util.ServerHelper
import net.minecraft.block.ITileEntityProvider
import net.minecraft.block.material.Material
import net.minecraft.client.renderer.texture.IIconRegister
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.{IBlockAccess, World}
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.fluids._

class FluidTransporter extends CoreBlock("fluidTransporter", Material iron) with ITileEntityProvider {
  registerTile(classOf[TileEntityFluidTransporter])

  override def getRenderBlockPass: Int = 1

  override def registerBlockIcons(reg: IIconRegister): Unit = {
    blockIcon = reg.registerIcon("fluidstuffs:fluidTransporter")
  }

  override def createNewTileEntity(world: World, int: Int): TileEntity = new TileEntityFluidTransporter;

  override def onBlockActivated(world: World, x: Int, y: Int, z: Int, player: EntityPlayer, par6: Int, par7: Float, par8: Float, par9: Float): Boolean = {
    val current = player.inventory.getCurrentItem
    def consumeItem(stack: ItemStack) = {
      if (stack.stackSize == 1) {
        if (stack.getItem().hasContainerItem(stack))
          stack.getItem().getContainerItem(stack)
        else
          null
      } else {
        stack.splitStack(1)
        stack
      }
    }
    if (current != null) {
      val liquid = FluidContainerRegistry.getFluidForFilledItem(current)
      val tile = world.getTileEntity(x, y, z).asInstanceOf[TileEntityFluidTransporter]
      if (liquid != null) {
        val ammout = tile.fill(ForgeDirection.UNKNOWN, liquid, false)
        if (ammout == liquid.amount) {
          tile.fill(ForgeDirection.UNKNOWN, liquid, true)
          if (!player.capabilities.isCreativeMode) {
            player.inventory.setInventorySlotContents(player.inventory.currentItem, consumeItem(current))
          }
          true
        } else true
      } else if (FluidContainerRegistry.isBucket(current)) {
        val tankInfo = tile.getTankInfo(ForgeDirection.UNKNOWN)
        val fillFluid = tankInfo.apply(0).fluid
        val fillStack = FluidContainerRegistry fillFluidContainer(fillFluid, current)
        if (fillStack != null) {
          tile.drain(ForgeDirection.UNKNOWN, FluidContainerRegistry.getFluidForFilledItem(fillStack).amount, true)
         if (!player.capabilities.isCreativeMode) {
            if (current.stackSize == 1) {
              player.inventory.setInventorySlotContents(player.inventory.currentItem, fillStack)
            } else {
              player.inventory.setInventorySlotContents(player.inventory.currentItem, consumeItem(current))
              if (!player.inventory.addItemStackToInventory(fillStack)) {
                player.dropPlayerItemWithRandomChoice(fillStack, false)
              }
            }
          }
          true
        }
      } else return super.onBlockActivated(world, x, y, z, player, par6, par7, par6, par9)
    } else false
    true
  }

  override def getRenderType: Int = RenderFluidTransporter.modelId

  override def renderAsNormalBlock(): Boolean = false

  override def isOpaqueCube: Boolean = false

  override def getLightValue(world: IBlockAccess, x: Int, y: Int, z: Int): Int = {
    val tile = world.getTileEntity(x, y, z).asInstanceOf[TileEntityFluidTransporter]
    if (tile.syncTank.containsFluid()) {
      tile.syncTank.getFluid.getFluid.getLuminosity(tile.syncTank.getFluid)
    } else {
      super.getLightValue(world, x, y, z)
    }
  }
}


