package adam8234.fluidstuffs.client

import adam8234.fluidstuffs.block.TileEntityFluidTransporter
import codechicken.lib.render.uv.IconTransformation
import codechicken.lib.render.{CCModel, CCRenderState, RenderUtils}
import codechicken.lib.vec.{Cuboid6, Translation, Vector3}
import cofh.util.MathHelper
import cpw.mods.fml.client.registry.{ISimpleBlockRenderingHandler, RenderingRegistry}
import net.minecraft.block.Block
import net.minecraft.client.renderer.RenderBlocks
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.IBlockAccess
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.fluids.FluidContainerRegistry

class RenderFluidTransporter extends TileEntitySpecialRenderer with ISimpleBlockRenderingHandler {

  override def renderInventoryBlock(block: Block, metadata: Int, modelId: Int, renderer: RenderBlocks): Unit = {
    CCRenderState reset()
    CCRenderState useNormals = true
    CCRenderState startDrawing
    val cube = CCModel.quadModel(24).generateBlock(0, new Cuboid6(new Vector3(0, 0, 0), new Vector3(1, 1, 1))).apply(new IconTransformation(block.getIcon(0, metadata))).computeNormals().render(new Translation(-.5, -.5, -.5))
    CCRenderState draw
  }

  override def getRenderId: Int = RenderFluidTransporter.modelId

  override def shouldRender3DInInventory(modelId: Int): Boolean = true

  override def renderWorldBlock(world: IBlockAccess, x: Int, y: Int, z: Int, block: Block, modelId: Int, renderer: RenderBlocks): Boolean = {
    renderer.renderStandardBlock(block, x, y, z)
    true
  }

  override def renderTileEntityAt(tile: TileEntity, x: Double, y: Double, z: Double, partialTickTime: Float): Unit = {
    var fluidTrans: TileEntityFluidTransporter = null
    if (tile.isInstanceOf[TileEntityFluidTransporter]) {
      fluidTrans = tile.asInstanceOf[TileEntityFluidTransporter]
    } else {
      return
    }
    CCRenderState reset()
    CCRenderState pullLightmap()

    if (fluidTrans.syncTank.containesFluid()) {
      RenderUtils renderFluidCuboid(fluidTrans.syncTank.getInfo.fluid, new Cuboid6(0.001, 0.001, 0.001, .999, .999, .999) add (new Vector3(x, y, z)), fluidTrans.clientAmount / (16D * FluidContainerRegistry.BUCKET_VOLUME), 1)
    }

  }

}

object RenderFluidTransporter {
  val modelId = RenderingRegistry getNextAvailableRenderId
}

