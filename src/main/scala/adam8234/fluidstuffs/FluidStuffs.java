package adam8234.fluidstuffs;

import adam8234.fluidstuffs.common.Proxy;
import adam8234.fluidstuffs.handler.EventHandler;
import adam8234.fluidstuffs.handler.GuiHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid = "fluidStuffs", version = "1.7.10-0.1")
public class FluidStuffs {

    public FluidStuffs() {
        MinecraftForge.EVENT_BUS.register(new EventHandler());
    }

    @Mod.Instance("fluidStuffs")
    public static FluidStuffs instance;

    @Mod.EventHandler
    public static void init(FMLInitializationEvent e) {
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
        Proxy.init();
    }

    @Mod.EventHandler
    public static void preInit(FMLPreInitializationEvent e) {
        Proxy.preInit();
    }

    @Mod.EventHandler
    public static void posInit(FMLPostInitializationEvent e) {
        Proxy.postInit();
    }
}
